#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/shm.h>
#include <signal.h>

//declaraciones
#define n 27
#define NUMERO 123
#define ARCHIVO "/bin/more"
//variables para memoria compartida
key_t Clave;
int Id_Memoria1;
int *Memoria1 = NULL;
//variables para distinguir lectura/escritura de proceso A o B
int A=0; 
int B=1;
//variables bandera
int sig2=0;
int alto=0;

//funcion escritura 
void escritura(int d)
{
  //funcion de cifrado (a.b) claves publicas
  int cifrado(int x, int a,int b) 
  {
   return (a*x+b) % n;
  }

  if (d==A) //Escribe proceso A
  {
    for (int i = 0; i < 10; i++) {
      Memoria1[i] = cifrado(i,Memoria1[20],Memoria1[21]);
      printf("Proceso A %d escribiendo: Memoria1[%d]= %d\n",getpid(),i,Memoria1 [i]);
      sleep (1);
    }
  }

  else if (d==B) //Escribe proceso B
  {
    for (int i = 10; i < 20; i++) {
      Memoria1[i] = cifrado(i,Memoria1[20],Memoria1[21]);
      printf("Proceso B %d escribiendo: Memoria1[%d]= %d\n",getpid(),i,Memoria1 [i]);
      sleep (1);
    }
  }

  else
  {
    printf("No se reconocio el proceso");
    exit(4);
  }

}

//funcion lectura
void lectura(int d){

    // Función de descifrado (k,c) claves privadas
    int descifrado(int x) {
     int k=7;
     int c=19;
     return (k*x+c) % n;
    }
    
    if(d==A) //lee proceso A
    {
      for (int i = 10; i < 20; i++) {
        printf("Proceso A %d leyendo Memoria1[%d]= %d\n", getpid(),i, descifrado(Memoria1[i]));
      }
    }

    else if (d==B) //lee proceso B
    {
      for (int i = 0; i < 10; i++) {
        printf("Proceso B %d leyendo: Memoria1[%d]= %d\n", getpid(),i, descifrado(Memoria1[i]));
      }
    }

    else{
      printf("No se reconocio el proceso");
      exit(5);
    }
}


////////////////////// PROGRAMA PRINCIPAL /////////////////////////////
int main(int argc, char *argv[]) {

  if (argc!=2){
    printf( "modo de uso: ./p2 [programaa|programab]\n");
    return 0;
  }
    
  // Crear memoria compartida
  Clave = ftok (ARCHIVO, NUMERO);
	if (Clave == -1)
	{
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

    Id_Memoria1 = shmget (Clave, 24*sizeof(int), 0666 | IPC_CREAT);
	if (Id_Memoria1 == -1)
	{
		printf("No consegui Id para memoria compartida\n");
		exit (2);
	}

    // Adjuntar la memoria compartida
    Memoria1 = (int *)shmat (Id_Memoria1, (const void *)0, 0);
	if (Memoria1 == NULL)
	{
		printf("No consegui asociar la memoria compartida a una variable\n");
		exit (3);
	}

    //Inicializo a y b claves publicas
    Memoria1[20]=4; //a
    Memoria1[21]=5; //b

    //A PARTIR DE ACA ACCIONES INDIVIDUALES PARA CADA PROCESO 
    //primer caso: escribe A y B-lee B lo de A- lee A lo de B
    //Bucle infinito: escribe A- lee B lo de A - escribe B - lee A lo de B

  if (strcmp(argv[1], "programaa") == 0) 
  {
    //funciones manejadores de señal p/sigterm-sigusr1
    void handler(int sig) {
      printf(" Se recibio la señal sigterm.\n");
      sig2=sig;
      alto=1;
    }
    void handlerA(int sig){  
    sig2=sig;
    }

    Memoria1[22]=getpid(); //PID proceso A
    signal(SIGTERM, handler); // Config señal SIGTERM
    signal(SIGUSR1, handlerA); // Config señal SIGUSR1

    //Comienzo de secuencia
    escritura(A); 
    printf("Esperando lectura de B\n");
    pause(); //Espera recibir una señal que envia proceso B
    while (sig2==15){
      pause();
    }
    lectura(A); //Lectura de B con A
    if(alto==1){ //Si se recibio la señal SIGTERM
      kill(Memoria1[23], SIGKILL);  //finalizo proceso B
      shmdt ((const void *) Memoria1); //Desvincular segmento de mem compartida
      shmctl (Id_Memoria1, IPC_RMID, (struct shmid_ds *)NULL); //Eliminar segmento de memoria compartida
      exit(0); //finalizo programa
    }

    while(1)
    {
      escritura(A); 
      printf("Esperando escritura de B\n");
      kill(Memoria1[23], SIGUSR2); //Envia señal SIGUSR2 al proceso B
      pause();
      while (sig2==15){
        pause();
      }
      lectura(A); //proceso A lee lo escrito por B
      if(alto==1){
        kill(Memoria1[23], SIGKILL);
        shmdt ((const void *) Memoria1);
        shmctl (Id_Memoria1, IPC_RMID, (struct shmid_ds *)NULL);
        exit(0);
      }
    }
       
  } 
    
  else if (strcmp(argv[1], "programab") == 0)
  {
    void handlerB(int sig){} //funciones manejadores de señal p/sigusr2

    signal(SIGUSR2, handlerB); //Config señal SIGUSR2
    Memoria1[23]= getpid(); //PID proceso B

    //Comienzo de secuencia
    escritura(B);
    lectura(B); // Lectura de A con B
    kill(Memoria1[22], SIGUSR1); //Envia señal SIGUSR1 al proceso A

    while(1){  
      printf("Esperando escritura de A\n");
      pause();  //Espera recibir señal enviada por proceso A
      lectura(B); //proceso B lee lo escrito por A
      escritura(B); 
      kill(Memoria1[22], SIGUSR1);
    }

  }

 return 0;
}
