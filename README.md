# README - Comunicación entre Procesos en C

Este proyecto en C demuestra la comunicación entre procesos utilizando memoria compartida y señales. A continuación, se presenta una descripción detallada de cómo se utilizan estas señales en el código:

## Descripción

El programa puede funcionar como proceso A o proceso B, dependiendo del parámetro pasado en la línea de comandos ("programaa" o "programab"). Esto afecta el comportamiento del programa y cómo interactúa con la memoria compartida.

### Inicialización de Variables y Funciones Manejadoras de Señales

El código comienza iniciando variables esenciales para ambos procesos. Para gestionar la memoria compartida, se incluyen las siguientes variables: la clave (Clave), el identificador de la memoria compartida (Id_Memoria1), y un puntero a un arreglo de enteros (Memoria1). Además, se utilizan las variables ‘A’ y ‘B’ para distinguir qué proceso lee o escribe. Asimismo, se emplean dos variables de bandera, ‘sig2’ y ‘alto’, que se inicializan en 0 y condicionan el flujo del programa.

### Función de escritura

La función “escritura” se encarga de escribir datos cifrados en la memoria compartida. Su funcionalidad varía según el proceso (A o B) que la esté ejecutando.

Se define una función interna llamada “cifrado”, que toma un valor pasado como parámetro ‘x’ para cifrar. Esta implementación la hace a través de la función con claves públicas almacenadas en la memoria compartida Memoria1[20] y Memoria1[21].

Si la función escritura la ejecuta el proceso A, se cifran los números del 0 al 9 en el rango 0-9 de Memoria1. En cambio, si la función la ejecuta el proceso B, se cifran los números del 10 al 19 en el rango 10-19 de Memoria1.

### Función de lectura

La función “lectura” se encarga de leer datos cifrados de la memoria compartida y descifrarlos. Su funcionalidad varía según el proceso (A o B) que la esté ejecutando.

Para ello, se define la función interna “descifrado”, que toma un valor pasado como parámetro ‘x’ para descifrar. Esta implementación la hace a través de una función donde sus variables son claves privadas solo conocidas por el proceso lector.

### Creación de memoria compartida

Se utiliza la función “ftok” para obtener una clave única a partir de un archivo (ARCHIVO) y un número (NÚMERO). Luego, se emplea la función ‘shmget’ para crear un segmento de memoria compartida de 24 enteros, siendo 20 de ellos asignados para el arreglo y 4 de ellos para variables auxiliares. Se asigna el resultado a Id_Memoria1.

Posteriormente, se utiliza la función ‘shmat’ para adjuntar el segmento de memoria compartida al proceso actual, lo que permite que el proceso acceda y modifique los datos en la memoria compartida. El puntero resultante se almacena en Memoria1.

Además, se inicializan dos valores en el segmento de memoria compartida, Memoria1[20] y Memoria1[21], que actúan como claves públicas para el cifrado.

#### Declaraciones del proceso A (programaa)

Se guarda el PID de este proceso A en Memoria1[22], que se utilizará para que el proceso B pueda enviarle señales a este proceso.

A su vez, se establecen manejadores de señales “handler” y “handlerA” para las señales SIGTERM y SIGUSR1 respectivamente. Dentro de estas funciones, se le asigna a ‘sig2’ el número de la señal correspondiente. Además, en la función “handler” se establece el valor de la variable “alto” en 1 para indicar que se recibió la señal SIGTERM y proceder a la finalización del programa.

#### Declaraciones del proceso B (programab)

Se establece el manejador de señales “handlerB” para la señal SIGUSR2.

Se guarda el PID de este proceso B en Memoria1[23], que se utilizará para que el proceso A pueda enviarle señales a este proceso.

#### Interacción entre procesos y bucle infinito

Ambos programas se inicializan con una secuencia única que se repetirá por primera y única vez y luego ingresan a un bucle infinito que permite una comunicación continua a través de la memoria compartida.

- Escribe el proceso A y espera recibir señal SIGUSR1 que envía proceso B.
- Escribe el proceso B y lee lo escrito por A. Luego, envía señal SIGUSR1 al proceso A y se queda esperando recibir señal SIGUSR2.
- A recibe la señal y lee lo escrito por B. Luego verifica si se recibió la señal SIGTERM. Si no se recibió, escribe y envía señal SIGUSR2 al proceso B. Se queda esperando recibir señal SIGUSR1.
- B lee lo escrito por A y escribe. Luego, envía señal SIGUSR1 al proceso A y se queda esperando recibir señal SIGUSR2.
- Se repite paso 3 y 4 hasta recibir señal SIGTERM.


### Eliminación de recursos y finalización del programa

Cuando el proceso A recibe la señal SIGTERM finaliza al proceso B (kill (Memoria1[23], SIGKILL)), desvincula la memoria compartida (shmdt), y finalmente elimina la memoria compartida (shmctl). Esto garantiza que se liberen los recursos adecuadamente y el programa finalice.
